import { useEffect, useRef, useState } from 'react';
import { GeoJSON, TileLayer, useMap } from 'react-leaflet';
import iconMarker from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import L from 'leaflet';
import { DraggableMarker } from './DraggableMarker';
import MarkerClusterGroup from './MarkerClusterGroup';
import { ModalPopupMarker } from './ModalPopupMarker';

const icon = L.icon({
  iconRetinaUrl: iconMarker,
  iconUrl: iconMarker,
  shadowUrl: iconShadow
});

export const MapCustom = (props) => {
  // Set up the useMap hook in the descendant of MapContainer
  const map = useMap();
  const refGeoJson = useRef(null);
  const [position, setPosition] = useState([]);
  const [hasGeoJson, setHasGeoJson] = useState(false);
  const [dataMarker, setDataMarker] = useState(null);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const pointToLayer = (feature, latlng) => {
    return L.marker(latlng, { icon: icon });
  };

  /*   const Popup = ({ feature }) => {
    return (
      <div>
        <b>{feature.properties.type.name}</b>
        <br />
        <Badge color={'primary'}>{feature.properties.status.name}</Badge>
        <br />
        <span> del {dayjs(feature.properties.published_at).format('DD/MM/YYYY  HH:mm')}</span>
        <p className={'mt-0'}>{feature.properties.subject}</p>
      </div>
    );
  }; */

  const onEachFeature = (feature, layer) => {
    layer.on('click', function () {
      setDataMarker(feature);
      setShow(true);
    });
  };

  useEffect(() => {
    // Access the map instance:
    map.whenReady(() => {
      map.invalidateSize();
      setPosition(props.position);
      if (props.geoJson) {
        setHasGeoJson(true);
      }
      //  map.fitBounds(map.getBounds());
    });
  }, [props, map]);

  return (
    <>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {position && position.length ? (
        <DraggableMarker
          position={position}
          icon={icon}
          draggable={props.draggable}
        ></DraggableMarker>
      ) : null}

      {hasGeoJson ? (
        <MarkerClusterGroup chunkedLoading removeOutsideVisibleBounds={false}>
          <GeoJSON
            data={props.geoJson}
            pointToLayer={pointToLayer}
            onEachFeature={onEachFeature}
            ref={refGeoJson}
          />
        </MarkerClusterGroup>
      ) : null}
      {dataMarker && show ? (
        <ModalPopupMarker
          show={show}
          onClose={handleClose}
          properties={dataMarker}
        ></ModalPopupMarker>
      ) : null}
    </>
  );
};
