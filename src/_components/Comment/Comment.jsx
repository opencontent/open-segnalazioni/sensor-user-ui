import dayjs from 'dayjs';
import {
  Button,
  FormGroup,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  TextArea
} from 'design-react-kit';
import { t } from 'i18next';
import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { currentUserActions, detailPostActions } from '../../_store';

export { Comment };

function Comment(props) {
  const dispatch = useDispatch();
  const [userComment, setUserComment] = useState('');
  const { currentUser } = useSelector((x) => x.currentUser);
  const ref = useRef(null);

  // Modal
  const [open, toggleModal] = useState(false);
  const [message, setMessage] = useState(null);

  const toggle = () => toggleModal(!open);
  const openModal = (data) => {
    setMessage(message || props.comment.text);
    toggleModal(true);
  };

  useEffect(() => {
    if (props.comment) {
      let id = props.comment.creator;
      dispatch(currentUserActions.getUserById({ id }))
        .unwrap()
        .then((res) => {
          setUserComment(res);
        });
    }
  }, [dispatch, props.comment]);

  function saveComment() {
    toggle();
    const data = {
      postId: props.postId,
      commentId: props.comment.id,
      text: message
    };
    dispatch(detailPostActions.updateComment(data));
  }

  return (
    <div className={`cmp-card-latest-messages mb-3 ${props.operator ? 'has-bg-grey' : null}`}>
      <div
        className={`card shadow-sm px-4 pt-4 pb-4 ${
          props.operator ? 'text-end has-bg-grey' : null
        }`}
      >
        <span className="visually-hidden"></span>
        <div className="card-header border-0 p-0 m-0">
          <span className="date-xsmall">
            {t('created_at')} {dayjs(props.comment.published_at).format('DD/MM/YYYY  HH:mm')}
          </span>
          {props.comment.published_at !== props.comment.modified_at ? (
            <span>
              {' '}
              {' - '}
              <span className="date-xsmall">
                {t('updated_at')} {dayjs(props.comment.modified_at).format('DD/MM/YYYY  HH:mm')}
              </span>
            </span>
          ) : null}
          {userComment.id === currentUser.id ? (
            <span className={'float-right'}>
              <Icon
                color={'primary'}
                icon="it-pencil"
                onClick={() => openModal({ post_id: props.postId, comment_id: props.comment.id })}
              ></Icon>
            </span>
          ) : null}
        </div>
        <div className="card-body p-0 my-2">
          <h3 className="title-small-semi-bold t-primary m-0 mb-1">
            <span className="text-decoration-none" data-focus-mouse="false">
              {userComment.name}
            </span>
          </h3>
          <p className="text-paragraph text-truncate">{props.comment.text}</p>
        </div>
      </div>
      <Modal fade centered isOpen={open} toggle={toggle} labelledBy="modifyComment">
        <ModalHeader toggle={toggle} id="modifyComment">
          {t('update_comment_of')} {userComment.name}
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <TextArea
              name="text"
              id="message-text"
              rows={3}
              label={t('message') + ':'}
              onChange={(e) => setMessage(e.target.value)}
              value={message}
              innerRef={ref}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>
            {t('close')}
          </Button>
          <Button color="primary" onClick={() => saveComment()}>
            {t('save_changes')}
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}
