import { Icon } from 'design-react-kit';
import { useEffect, useState } from 'react';
import { useDispatch } from "react-redux";
import { formatBytes, resizeImage } from '../../_helpers/utilities';
import { uploadsActions } from '../../_store';

export { ImageList };

function ImageList({ img }) {
  //const [imagePreviewUrl, setImagePreviewUrl] = useState(null);
  const [imageResizedUrl, setImageResizedUrl] = useState(null);
  const [image, setImg] = useState(null);
  const [storeImgData, setStoreImg] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    if (img) {
      const extension = img.name.match(/\.[0-9a-z]+$/i);
      let reader = new FileReader();
      reader.readAsDataURL(img);
      reader.onload = function () {
        resizeImage(reader.result, 50, 50).then((result) => {
          const storeImg = {
            file: reader.result.replace(/data:.+?,/, ''),
            filename: 'img-' + Date.now() + extension[0]
          };
          setImg(reader.result);
          setStoreImg(storeImg)
          dispatch(uploadsActions.saveImage(storeImg));
          setImageResizedUrl(result);
        });
      };
      reader.onerror = function (error) {
        console.log('Error: ', error);
      };
    }
  }, [img, dispatch]);

  const handleDeleteImage = (storeImg) => {
    setImg(null);
    dispatch(uploadsActions.removeImage(storeImg.file));
  };

  return (
    <>
      {image && (
        <>
          <div
            className="upload-wrapper d-flex justify-content-between align-items-center"
            key={'img-'}
          >
            {imageResizedUrl && <img src={imageResizedUrl} alt={'img-resized'} />}
            {/* {imagePreviewUrl && <img src={imagePreviewUrl} alt={'img-preview'} />} */}

            <span className="t-primary fw-bold w-60 ms-2">{img.name}</span>
            <span className="upload-file-weight">{formatBytes(img.size)}</span>
            <span className="align-self-center">
              <Icon
                onClick={() => handleDeleteImage(storeImgData)}
                icon={'it-close'}
                size={'sm'}
                color={'primary'}
                className={'mb-1'}
              ></Icon>
            </span>
          </div>
          <hr />
        </>
      )}
    </>
  );
}
