import { Button, Header, HeaderBrand, HeaderContent, HeaderRightZone } from 'design-react-kit';
import { changeLanguage } from 'i18next';
import { useSelector, useDispatch } from 'react-redux';
import { authActions } from '../_store';

export function Nav() {
  const authUser = useSelector((x) => x.auth.user);
  const dispatch = useDispatch();
  const logout = () => dispatch(authActions.logout());

  // only show nav when logged in
  if (!authUser) return null;

  const test = () => {
    changeLanguage('en-US');
  };

  return (
    <>
      <Header theme="" type="slim">
        <HeaderContent className={'it-header-wrapper it-header-sticky'}>
          <HeaderBrand>React Sensor UI</HeaderBrand>
          <HeaderRightZone>
            <div className="it-access-top-wrapper">
              <Button color="primary" size="sm" onClick={logout}>
                Logout
              </Button>
            </div>
          </HeaderRightZone>
        </HeaderContent>
      </Header>
      <Header theme="" type="center">
        <HeaderContent>
          <HeaderBrand iconName="it-code-circle">
            <h2 onClick={test}>OpenSegnalazioni</h2>
            <h3>Il Comune di in ascolto</h3>
          </HeaderBrand>
        </HeaderContent>
      </Header>
    </>
  );
}
