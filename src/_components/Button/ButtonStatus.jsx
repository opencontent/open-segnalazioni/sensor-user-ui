import { t } from 'i18next';

export { ButtonsStatus };

function ButtonsStatus(props) {
  const status = props.status;
  if (status === 'complete') {
    return <ButtonStatusCompleted />;
  } else if (status === 'pending') {
    return <ButtonStatusPending />;
  } else if (status === 'close') {
    return <ButtonStatusClose />;
  } else {
    return <ButtonStatusOpen />;
  }
}

function ButtonStatusPending() {
  return (
    <div color="warning">
      {t('pending')}
      <span className="visually-hidden">{t('pending')}</span>
    </div>
  );
}

function ButtonStatusCompleted() {
  return (
    <div color="success">
      {t('completed')}
      <span className="visually-hidden">{t('completed')}</span>
    </div>
  );
}

function ButtonStatusClose() {
  return (
    <div color="success">
      {t('closed')}
      <span className="visually-hidden">{t('closed')}</span>
    </div>
  );
}

function ButtonStatusOpen() {
  return (
    <div color="danger">
      {t('opened')}
      <span className="visually-hidden">{t('opened')}</span>
    </div>
  );
}
