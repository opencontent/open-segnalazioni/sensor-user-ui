import { Col, Spinner } from 'design-react-kit';
import { useEffect, useState } from 'react';
import shortid from 'shortid';

export { ImageHeader };

function ImageHeader(props) {
  const [img, setImg] = useState(null);
  const imageUrl = props.props;
  const key = shortid.generate();

  useEffect(() => {
    const fetchImage = async () => {
      const res = await fetch(imageUrl);
      const imageBlob = await res.blob();
      const imageObjectURL = URL.createObjectURL(imageBlob);
      setImg(imageObjectURL);
    };

    fetchImage();
  }, [imageUrl]);

  if (imageUrl) {
    return (
      <div className={'col'}>
        {img ? (
          <img src={img ? img : ''} alt={'image-' + key} className="img-fluid mb-3 mb-lg-0" />
        ) : (
          <Col className="text-center p-5">
            <Spinner double active />
          </Col>
        )}
      </div>
    );
  } else {
    return (
      <Col className="text-center p-5">
        <Spinner double active />
      </Col>
    );
  }
}
