import dayjs from 'dayjs';
import { Button, Card, CardBody, CardFooterCTA, CardTag, CardTagsHeader } from 'design-react-kit';
import { t } from 'i18next';
import { Link, useNavigate } from 'react-router-dom';
import { capitalizeFirstLetter } from '../../_helpers/utilities';
import { ButtonsStatus } from '../Button/ButtonStatus';
import { ImageHeader } from '../Images/ImageHeader';

export { CardPost };

function CardPost(props) {
  const data = { ...props };

  let navigate = useNavigate();
  const routeChange = (id) => {
    let path = `/sensor/posts/${id}`;
    navigate(path);
  };

  return (
    <div className="cmp-card mb-4 mb-lg-30" key={data.props.id}>
      <Card className="has-bkg-grey shadow-sm">
        <CardBody className="p-0">
          <div className="cmp-info-button-card">
            <Card className="p-3 p-lg-4">
              <CardBody className="p-0">
                <CardTagsHeader
                  date={
                    t('published_at') +
                    ' ' +
                    dayjs(data.props.published_at).format('DD/MM/YYYY  HH:mm')
                  }
                >
                  <CardTag tag={'span'}>
                    <ButtonsStatus status={data.props.status}></ButtonsStatus>
                  </CardTag>
                </CardTagsHeader>
                <h3 className="medium-title mb-0">{data.props.subject}</h3>
                <p className="card-info">
                  {t('typology')} <br /> <span>{capitalizeFirstLetter(data.props.type)}</span>
                </p>
                <div className="accordion-item">
                  <div className="accordion-header">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target={`#collapse-${data.props.id}`}
                      aria-expanded="false"
                      aria-controls={`collapse-${data.props.id}`}
                      data-focus-mouse="false"
                    >
                      <span className="d-flex align-items-center">
                        {t('show_more')}
                        {/* <Icon color={'primary'} size={'sm'} icon={'it-expand'}></Icon> */}
                      </span>
                    </button>
                  </div>
                  <div
                    id={`collapse-${data.props.id}`}
                    className="accordion-collapse collapse"
                    role="region"
                  >
                    <div className="accordion-body p-0">
                      <div className="cmp-info-summary bg-white border-0">
                        <div className="card">
                          <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-end">
                            <Link to="#" className="d-none text-decoration-none">
                              <span className="text-button-sm-semi t-primary">{t('edit')}</span>
                            </Link>
                          </div>

                          <CardBody className="card-body p-0">
                            <div className="single-line-info border-light">
                              <div className="text-paragraph-small">{t('address')}</div>
                              <div className="border-light">
                                <p className="data-text">
                                  {data.props.address ? data.props.address?.address : '--'}
                                </p>
                              </div>
                            </div>
                            <div className="single-line-info border-light">
                              <div className="text-paragraph-small">{t('detail')}</div>
                              <div className="border-light">
                                <p className="data-text">{data.props.description}</p>
                              </div>
                            </div>
                            {data.props.images.length > 0 ? (
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('images')}</div>
                                <div className="border-light border-0">
                                  <div className="d-lg-flex gap-2 mt-3 row">
                                    {data.props.images.map((img, idx) => (
                                      <ImageHeader
                                        props={img}
                                        key={`image-${idx.toString()}`}
                                      ></ImageHeader>
                                    ))}
                                  </div>
                                </div>
                              </div>
                            ) : null}
                          </CardBody>
                          <CardFooterCTA className={'mt-2'}>
                            <Button
                              outline
                              color="primary"
                              onClick={() => {
                                routeChange(data.props.id);
                              }}
                              className="mr-3"
                            >
                              {t('show_all')}
                            </Button>
                          </CardFooterCTA>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </CardBody>
      </Card>
    </div>
  );
}
