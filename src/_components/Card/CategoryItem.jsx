import { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { postsActions } from '../../_store';

export { CategoryItem };

function CategoryItem(props) {
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const [isChecked, setIsChecked] = useState(false);
  const [countPost, setCountPost] = useState('');
  const { filters } = useSelector((x) => x.posts);

  const handleFilterCategory = (id, event) => {
    if (event.currentTarget.checked) {
      setIsChecked(event.currentTarget.checked);
      dispatch(postsActions.addFiltersCategory(id));
    } else {
      setIsChecked(event.currentTarget.checked);
      dispatch(postsActions.removeFiltersCategory(id));
    }
  };

  useEffect(() => {
    if (props.reset) {
      setIsChecked(false);
      props.setCallables(false);
    }
  }, [props]);

  useEffect(() => {
    if (props.props.id) {
      dispatch(postsActions.getCategoryById(props.props.id))
        .unwrap()
        .then((res) => {
          setCountPost(res.count);
        });
    }
  }, [dispatch, props.props.id]);

  return (
    <li>
      <div className="form-check">
        <div className="checkbox-body border-light py-3">
          <input
            type="checkbox"
            id={props.props.id}
            onChange={(event) => handleFilterCategory(props.props.id, event)}
            name="category"
            value={props.props.id}
            checked={isChecked || filters.categories.toString().includes(props.props.id)}
            ref={inputRef}
          />
          <label
            htmlFor={props.props.id}
            className="subtitle-small_semi-bold mb-0 category-list__list"
          >
            {props.props.name} {countPost ? `(${countPost})` : ''}
          </label>
        </div>
      </div>
    </li>
  );
}
