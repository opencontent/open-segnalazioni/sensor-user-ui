import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Col,
  Container,
  FormGroup,
  Icon,
  Input,
  Label,
  Nav,
  NavItem,
  NavLink,
  Row,
  Spinner,
  TabContent,
  TabPane
} from 'design-react-kit';

import { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { MapContainer } from 'react-leaflet';

import { useSelector, useDispatch } from 'react-redux';

import { useNavigate } from 'react-router-dom';
import { CardPost } from '../_components/Card/CardPost';
import { CategoryItem } from '../_components/Card/CategoryItem';
import { MapCustom } from '../_components/Map/MapCustom';
import { ModalNewPost } from '../_components/Modal/ModalNewPost';
import { currentUserActions, postsActions } from '../_store';

export { Home };

function Home(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const ref = useRef(null);
  const [enableMap, setEnableMap] = useState(false);
  const { currentUser } = useSelector((x) => x.currentUser);

  const { t } = useTranslation();

  const { posts, newItems, categories, filters, geoJson, stats } = useSelector((x) => x.posts);

  const [activeTab, toggleTab] = useState('1');
  let [reset, setReset] = useState(false);

  useEffect(() => {
    if (filters.auth) {
      dispatch(postsActions.getGeoJsonPosts({ auth: true, categories: filters.categories.join() }));
      loadMap();
    } else {
      dispatch(postsActions.getGeoJsonPosts({ categories: filters.categories.join() }));
      loadMap();
    }
  }, [dispatch, filters]);

  useEffect(() => {
    if (filters.categories.length) {
      if (filters.auth) {
        dispatch(postsActions.addFilterMyPosts(filters.categories.join()));
      } else {
        dispatch(postsActions.filterPosts(filters.categories.join()));
      }
    } else {
      if (filters.auth) {
        dispatch(postsActions.myPosts());
      } else {
        dispatch(postsActions.getAll());
      }
    }
  }, [dispatch, filters]);

  useEffect(() => {
    dispatch(postsActions.getCategories());
    dispatch(currentUserActions.getCurrentUser());
    dispatch(postsActions.getStats());
  }, [dispatch]);

  const onClickNextPost = (next) => {
    if (next) {
      dispatch(postsActions.getOtherPosts({ next }), posts).unwrap();
    }
  };

  const loadMap = () => {
    setEnableMap(true);
  };

  const handleRemoveAllFilters = (event) => {
    dispatch(postsActions.filterMyPost(false));
    dispatch(postsActions.removeAllFiltersCategory());
    setChildCallables();
  };

  const handleMyPost = (event) => {
    // Toggle filter for my posts
    dispatch(postsActions.filterMyPost(event.target.checked));
    dispatch(postsActions.myPosts());
  };

  const setChildCallables = () => {
    setReset(true);
  };

  const pull_data = () => {
    if (reset) {
      setReset(false);
    }
  };

  return (
    <Container className="px-4 my-4" id="intro" tag="section">
      <Row className="justify-content-center">
        <Col className="col-12" lg={10}>
          <div className="cmp-breadcrumbs" role="navigation">
            <Breadcrumb>
              <BreadcrumbItem active>
                <a href="/">{t('reports')}</a>
                <span className="separator"></span>
              </BreadcrumbItem>
            </Breadcrumb>
          </div>
          <h1>{t('report_list')}</h1>
          <p className="subtitle-small">
            Negli ultimi 12 mesi sono state risolte {stats} segnalazioni.{' '}
          </p>
        </Col>
      </Row>
      <Row className="mb-md-40 mb-lg-80">
        <Col>
          <hr className="d-none d-lg-block mt-30 mb-2" />
        </Col>
      </Row>
      <Row className={'mb-md-40 mb-lg-80'}>
        <Col lg={3}>
          {/* <NotificationManager fix="right-fix" /> */}
          {/*           <div className="floating-container">
            {!props.onScroll ? (
              <div className="floating-button">
                <Forward scrollToRef={ref}>
                  <Icon color="light" icon="it-expand" />
                </Forward>
              </div>
            ) : (
              ''
            )}
            {props.goToTop ? (
              <div className="floating-button" onClick={goToTop}>
                <Icon color="light" icon="it-collapse" />
              </div>
            ) : (
              ''
            )}
          </div> */}
          <fieldset>
            <div className="pb-4">
              <div className="w-100 d-flex justify-content-between border-bottom border-light pb-3 mt-5">
                <span className="h6 text-uppercase category-list__title">{t('category')}</span>
                {filters.categories.length > 0 ? (
                  <span>
                    <button
                      type="button"
                      onClick={handleRemoveAllFilters}
                      className="btn p-0 pe-2 d-none d-lg-block"
                    >
                      <span className="title-xsmall-semi-bold ms-1">{t('remove_all_filters')}</span>
                    </button>
                    <button type="button" className="btn p-0 pe-2 d-lg-none">
                      <span className="rounded-icon"></span>
                      <span className="t-primary title-xsmall-semi-bold ms-1">{t('filter')}</span>
                    </button>
                  </span>
                ) : null}
              </div>
              {categories.count ? (
                <ul>
                  {categories.items
                    .filter((el) => el.parent === 0)
                    .map((cat, index) => (
                      <CategoryItem
                        props={cat}
                        key={cat.id + index.toString()}
                        setCallables={pull_data}
                        reset={reset}
                      ></CategoryItem>
                    ))}
                </ul>
              ) : (
                <Col className="text-center p-5">
                  <Spinner double active />
                </Col>
              )}
            </div>
          </fieldset>
        </Col>
        <Col lg={8} className={'offset-lg-1'}>
          <Row>
            <div className="back-to-top">
              <Icon color="primary" icon="it-arrow-up" />
            </div>
            <div className="w-100 d-flex justify-content-between border-bottom border-light pb-3 mt-5">
              <span className="search-results">
                {posts.count ? `${t('results')} ${posts.count}` : ''}
              </span>
              {currentUser && currentUser.last_access_at !== null ? (
                <FormGroup check className={'mt-0'}>
                <Input
                  id="checkboxMyPosts"
                  type="checkbox"
                  {...(filters.auth && { checked: true })}
                  onChange={handleMyPost}
                />
                <Label for="checkboxMyPosts" className={'mb-0 lh-auto'}>
                  {t('my_reports')}
                </Label>
              </FormGroup>
                ) : ''}
            </div>
            <Nav
              tabs
              className="w-100 flex-nowrap border-bottom border-light mb-40 mt-3 shadow-none"
            >
              <NavItem className="w-100">
                <NavLink
                  href="#"
                  active={activeTab === '1'}
                  className="title-medium-semi-bold pt-0"
                  onClick={(e) => {
                    e.preventDefault();
                    if (activeTab !== '1') {
                      toggleTab('1');
                    }
                    loadMap();
                  }}
                >
                  {t('map')}
                </NavLink>
              </NavItem>
              <NavItem className="w-100">
                <NavLink
                  href="#"
                  active={activeTab === '2'}
                  className="title-medium-semi-bold pt-0"
                  onClick={(e) => {
                    e.preventDefault();
                    if (activeTab !== '2') {
                      toggleTab('2');
                    }
                  }}
                >
                  {t('list')}
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={activeTab} className={'w-100'}>
              <TabPane tabId="1" className="p-3">
                {geoJson && geoJson.features && enableMap ? (
                  <MapContainer
                    center={[44.7510665, 11.7575136]}
                    zoom={6}
                    scrollWheelZoom={false}
                    style={{ height: '600px' }}
                  >
                    <MapCustom draggable={false} geoJson={geoJson} />
                  </MapContainer>
                ) : (
                  <Col lg={12} className="text-center p-5">
                    <Spinner double active />
                  </Col>
                )}
              </TabPane>
              <TabPane tabId="2" className="p-3">
                {posts.count > 0 ? (
                  <div>
                    {posts.items.map((post, index) => (
                      <CardPost props={post} key={post.id + index.toString()}></CardPost>
                    ))}
                    <div ref={ref}></div>
                  </div>
                ) : null}
                {posts.count === 0 ? (
                  <div>
                    <h3>{t('no_reports')}</h3>
                  </div>
                ) : null}

                {posts?.loading || newItems?.loading ? (
                  <Col lg={12} className="text-center">
                    <Spinner double active />
                  </Col>
                ) : null}
                {posts.next && !newItems?.loading && (
                  <Col lg={12} className="text-center">
                    <Button
                      outline
                      color="primary"
                      type="button"
                      onClick={() => onClickNextPost(posts.next)}
                      className="mobile-full py-3 mt-10 mx-auto"
                    >
                      {t('load_more_reports')}
                    </Button>
                  </Col>
                )}
                <div ref={ref}></div>
              </TabPane>
            </TabContent>
          </Row>
          {currentUser && currentUser.last_access_at !== null ? (
            <Col lg={6} className="mt-50 mb-4 mb-lg-0">
              <div className="cmp-text-button mt-0" onClick={() => navigate('/sensor/posts/new')}>
                <h2 className="title-xxlarge mb-0">{t('make_a_report')}</h2>
                <div className="text-wrapper">
                  <p className="subtitle-small mb-3 mt-3">{t('make_a_report_description')}</p>
                </div>
                <ModalNewPost></ModalNewPost>
              </div>
            </Col>
          ) : null}
        </Col>
      </Row>
    </Container>
  );
}
