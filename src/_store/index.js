import { configureStore } from '@reduxjs/toolkit';
import { areasReducer } from './areas.slice';

import { authReducer } from './auth.slice';
import { detailPostReducer } from './detailpost.slice';
import { postsReducer } from './posts.slice';
import { currentUserReducer } from './currentuser.slice';
import { uploadsReducer } from './uploads.slice';

export * from './auth.slice';
export * from './posts.slice';
export * from './currentuser.slice';
export * from './detailpost.slice';
export * from './uploads.slice';
export * from './areas.slice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    posts: postsReducer,
    currentUser: currentUserReducer,
    detailPost: detailPostReducer,
    images: uploadsReducer,
    areas: areasReducer
  }
});
