import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice

const name = 'areas';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/getAll/pending`, (state, { meta }) => {
        state.areas = { loading: true };
      })
      .addCase(`${name}/getAll/fulfilled`, (state, action) => {
        state.areas = action.payload;
      })
      .addCase(`${name}/getAll/rejected`, (state, action) => {
        state.areas = { error: action.error };
      });
  }
});

// exports

export const areasActions = { ...slice.actions, ...extraActions };
export const areasReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    areas: {},
    address: {}
  };
}

function createExtraActions() {
  const baseUrl = `${process.env.REACT_APP_API_URL}/sensor`;

  return {
    getAll: getAll()
  };

  function getAll() {
    return createAsyncThunk(
      `${name}/getAll`,
      async () => await fetchWrapper.get(`${baseUrl}/areas?limit=50`)
    );
  }
}
