import { createAsyncThunk, createSlice, current } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice

const name = 'detailPost';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/getDetailPost/pending`, (state, { meta }) => {
        state.detailPost = { loading: true };
      })
      .addCase(`${name}/getDetailPost/fulfilled`, (state, action) => {
        state.detailPost = action.payload;
      })
      .addCase(`${name}/getDetailPost/rejected`, (state, action) => {
        state.detailPost = { error: action.error };
      })
      .addCase(`${name}/getAddress/pending`, (state, { meta }) => {
        state.address = { loading: true };
      })
      .addCase(`${name}/getAddress/fulfilled`, (state, action) => {
        state.address = action.payload;
      })
      .addCase(`${name}/getAddress/rejected`, (state, action) => {
        state.address = { error: action.error };
      })
      .addCase(`${name}/updateComment/pending`, (state, { meta }) => {
        //state.detailPost = { loading: true };
      })
      .addCase(`${name}/updateComment/fulfilled`, (state, action) => {
        const currentState = current(state);
        const payload = [action.payload];
        state.detailPost._embedded.comments = currentState.detailPost._embedded.comments.map(
          (obj) => payload.find((o) => o.id === obj.id) || obj
        );
      })
      .addCase(`${name}/updateComment/rejected`, (state, action) => {
        //state.detailPost = { error: action.error }
      })
      .addCase(`${name}/createComment/pending`, (state, { meta }) => {
        //state.detailPost = { loading: true };
      })
      .addCase(`${name}/createComment/fulfilled`, (state, action) => {
        const currentState = current(state);
        const payload = [action.payload];
        state.detailPost._embedded.comments = currentState.detailPost._embedded.comments.map(
          (obj) => payload.find((o) => o.id === obj.id) || obj
        );
      })
      .addCase(`${name}/createComment/rejected`, (state, action) => {
        //state.detailPost = { error: action.error }
      });
  }
});

// exports

export const detailPostActions = { ...slice.actions, ...extraActions };
export const detailPostReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    detailPost: {},
    address: {}
  };
}

function createExtraActions() {
  const baseUrl = `${process.env.REACT_APP_API_URL}/sensor`;

  return {
    getDetailPost: getDetailPost(),
    getAddress: getAddress(),
    updateComment: updateComment(),
    createComment: createComment()
  };

  function getDetailPost() {
    return createAsyncThunk(
      `${name}/getDetailPost`,
      async (arg) =>
        await fetchWrapper.get(
          `${baseUrl}/posts/${arg.id}?embed=comments,privateMessages,responses,attachments,timeline,areas,categories,approvers,owners,observers`
        )
    );
  }

  function getAddress() {
    return createAsyncThunk(`${name}/getAddress`, (arg) => arg);
  }

  function updateComment() {
    return createAsyncThunk(
      `${name}/updateComment`,
      async (params) =>
        await fetchWrapper.put(
          `${baseUrl}/posts/${params.postId}/comments/${params.commentId}`,
          params
        )
    );
  }

  function createComment() {
    return createAsyncThunk(
      `${name}/createComment`,
      async (params) =>
        await fetchWrapper.post(`${baseUrl}/posts/${params.postId}/comments`, params)
    );
  }
}
