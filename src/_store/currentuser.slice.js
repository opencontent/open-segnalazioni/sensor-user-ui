import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice

const name = 'currentUser';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/getCurrentUser/pending`, (state) => {
        state.currentUser = { loading: true };
      })
      .addCase(`${name}/getCurrentUser/fulfilled`, (state, action) => {
        state.currentUser = action.payload;
      })
      .addCase(`${name}/getCurrentUser/rejected`, (state, action) => {
        state.currentUser = { error: action.error };
      })
      .addCase(`${name}/getUserById/pending`, (state) => {
        state.user = { loading: true };
      })
      .addCase(`${name}/getUserById/fulfilled`, (state, action) => {
        state.user = action.payload;
      })
      .addCase(`${name}/getUserById/rejected`, (state, action) => {
        state.user = { error: action.error };
      });
  }
});

// exports
export const currentUserActions = { ...slice.actions, ...extraActions };
export const currentUserReducer = slice.reducer;

// implementation
function createInitialState() {
  return {
    currentUser: {},
    user: {}
  };
}

function createExtraActions() {
  const baseUrl = `${process.env.REACT_APP_API_URL}/sensor`;

  return {
    getCurrentUser: getCurrentUser(),
    getUserById: getUserById()
  };

  function getCurrentUser() {
    return createAsyncThunk(
      `${name}/getCurrentUser`,
      async () => await fetchWrapper.get(`${baseUrl}/users/current`)
    );
  }

  function getUserById() {
    return createAsyncThunk(
      `${name}/getUserById`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/users/${arg.id}`)
    );
  }
}
