import { createAsyncThunk, createSlice, current } from "@reduxjs/toolkit";

// create slice

const name = 'uploads';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/saveImage/pending`, (state, { meta }) => {
        //state.images = { loading: true };
      })
      .addCase(`${name}/saveImage/fulfilled`, (state, action) => {
        state.images.push(action.payload);
      })
      .addCase(`${name}/saveImage/rejected`, (state, action) => {
       // state.images = { error: action.error };
      })

  .addCase(`${name}/removeImage/pending`, (state, { meta }) => {
      //state.images = { loading: true };
    })
    .addCase(`${name}/removeImage/fulfilled`, (state, action) => {
      const currentState = current(state);
      state.images =  currentState.images.filter(el => el.file !== action.payload);
      console.log('img',state.images)
    })
    .addCase(`${name}/removeImage/rejected`, (state, action) => {
      //state.images = { error: action.error };
    });
  }
});

// exports

export const uploadsActions = { ...slice.actions, ...extraActions };
export const uploadsReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    images: []
  };
}

function createExtraActions() {
  return {
    saveImage: saveImage(),
    removeImage: removeImage()
  };

  function saveImage() {
    return createAsyncThunk(`${name}/saveImage`, (arg) => arg);
  }

  function removeImage() {
    return createAsyncThunk(`${name}/removeImage`, (arg) => arg);
  }
}
