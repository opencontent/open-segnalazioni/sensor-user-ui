import { createAsyncThunk, createSlice, current } from '@reduxjs/toolkit';

import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice

const name = 'posts';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder

      /** getAll **/
      .addCase(`${name}/getAll/pending`, (state, { meta }) => {
        state.posts = { loading: true };
      })
      .addCase(`${name}/getAll/fulfilled`, (state, action) => {
        state.posts = action.payload;
      })
      .addCase(`${name}/getAll/rejected`, (state, action) => {
        state.posts = { error: action.error };
      })

      /** myPosts **/
      .addCase(`${name}/myPosts/pending`, (state, { meta }) => {
        state.posts = { loading: true };
      })
      .addCase(`${name}/myPosts/fulfilled`, (state, action) => {
        state.posts = action.payload;
      })
      .addCase(`${name}/myPosts/rejected`, (state, action) => {
        state.posts = { error: action.error };
      })

      /** getOtherPosts **/
      .addCase(`${name}/getOtherPosts/pending`, (state, { meta }) => {
        state.newItems = { loading: true };
      })
      .addCase(`${name}/getOtherPosts/fulfilled`, (state, action) => {
        const currentState = current(state);
        const newItems = [...currentState.posts.items, ...action.payload.items];
        state.posts = { ...action.payload };
        state.posts.items = newItems;
        state.newItems = { loading: false };
      })
      .addCase(`${name}/getOtherPosts/rejected`, (state, action) => {
        state.newItems = { error: action.error };
      })

      /** getType **/
      .addCase(`${name}/getType/pending`, (state, { meta }) => {
        state.types = { loading: true };
      })
      .addCase(`${name}/getType/fulfilled`, (state, action) => {
        state.types = { ...action.payload };
      })
      .addCase(`${name}/getType/rejected`, (state, action) => {
        state.types = { error: action.error };
      })

      /** getZone **/
      .addCase(`${name}/getZone/pending`, (state, { meta }) => {
        state.zones = { loading: true };
      })
      .addCase(`${name}/getZone/fulfilled`, (state, action) => {
        state.zones = { ...action.payload };
      })
      .addCase(`${name}/getZone/rejected`, (state, action) => {
        state.zones = { error: action.error };
      })

      /** getCategories **/
      .addCase(`${name}/getCategories/pending`, (state, { meta }) => {
        state.categories = { loading: true };
      })
      .addCase(`${name}/getCategories/fulfilled`, (state, action) => {
        state.categories = { ...action.payload };
      })
      .addCase(`${name}/getCategories/rejected`, (state, action) => {
        state.categories = { error: action.error };
      })

      /** addFiltersPost **/
      .addCase(`${name}/addFiltersPost/pending`, (state, { meta }) => {
        state.posts = { loading: true };
      })
      .addCase(`${name}/addFiltersPost/fulfilled`, (state, action) => {
        state.posts = action.payload;
      })
      .addCase(`${name}/addFiltersPost/rejected`, (state, action) => {
        state.posts = { error: action.error };
      })

      /** addFiltersCategory **/
      .addCase(`${name}/addFiltersCategory/pending`, (state, { meta }) => {})

      .addCase(`${name}/addFiltersCategory/fulfilled`, (state, action) => {
        const currentState = current(state);
        let categories = [...currentState.filters.categories];
        categories.push(action.payload);
        state.filters = {
          categories: categories,
          auth: currentState.filters.auth
        };
      })
      .addCase(`${name}/addFiltersCategory/rejected`, (state, action) => {
        state.filters = { error: action.error };
      })

      /** removeFiltersCategory **/
      .addCase(`${name}/removeFiltersCategory/pending`, (state, { meta }) => {})
      .addCase(`${name}/removeFiltersCategory/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.filters = {
          categories: currentState.filters.categories.filter((item) => item !== action.payload)
        };
      })
      .addCase(`${name}/removeFiltersCategory/rejected`, (state, action) => {
        state.filters = { error: action.error };
      })

      /** filterMyPost **/
      .addCase(`${name}/filterMyPost/pending`, (state, { meta }) => {})
      .addCase(`${name}/filterMyPost/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.filters = {
          categories: currentState.filters.categories,
          auth: action.payload
        };
      })
      .addCase(`${name}/filterMyPost/rejected`, (state, action) => {
        const currentState = current(state);
        state.filters = {
          categories: currentState.filters.categories,
          auth: currentState.filters.auth
        };
      })

      /** removeAllFiltersCategory **/
      .addCase(`${name}/removeAllFiltersCategory/pending`, (state, { meta }) => {
        state.filters = {
          categories: [],
          auth: false
        };
      })
      .addCase(`${name}/removeAllFiltersCategory/fulfilled`, (state, action) => {
        state.filters = {
          categories: [],
          auth: false
        };
      })
      .addCase(`${name}/removeAllFiltersCategory/rejected`, (state, action) => {
        state.filters = { error: action.error };
      })

      /** getGeoJsonPosts **/
      .addCase(`${name}/getGeoJsonPosts/pending`, (state, { meta }) => {
        state.geoJson = { loading: true };
      })
      .addCase(`${name}/getGeoJsonPosts/fulfilled`, (state, action) => {
        state.geoJson = action.payload;
      })
      .addCase(`${name}/getGeoJsonPosts/rejected`, (state, action) => {
        state.geoJson = { error: action.error };
      })

      /** createPost **/
      .addCase(`${name}/createPost/pending`, (state, { meta }) => {
        const currentState = current(state);
        state.posts = {
          ...currentState.posts.items,
          loading: true,
          error: false
        };
      })
      .addCase(`${name}/createPost/fulfilled`, (state, action) => {
        const currentState = current(state);
        state.posts = {
          ...currentState.posts.items,
          loading: false,
          error: false
        };
      })
      .addCase(`${name}/createPost/rejected`, (state, action) => {
        const currentState = current(state);
        state.posts = {
          ...currentState.posts.items,
          loading: false,
          error:true
        };
      })

      /** getStats **/
      .addCase(`${name}/getStats/pending`, (state, { meta }) => {})
      .addCase(`${name}/getStats/fulfilled`, (state, action) => {
        state.stats = action.payload.flat()[0][1].toString() || null;
      })
      .addCase(`${name}/getStats/rejected`, (state, action) => {})
      /** getCategoryById **/
      .addCase(`${name}/getCategoryById/pending`, (state, { meta }) => {})
      .addCase(`${name}/getCategoryById/fulfilled`, (state, action) => {})
      .addCase(`${name}/getCategoryById/rejected`, (state, action) => {});
  }
});

// exports
export const postsActions = { ...slice.actions, ...extraActions };
export const postsReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    posts: {
      items: [],
      loading: false,
      error: false
    },
    newItems: null,
    types: [],
    zones: [],
    categories: [],
    filters: {
      auth: false,
      categories: []
    },
    geoJson: [],
    stats: []
  };
}

function createExtraActions() {
  const baseUrl = `${process.env.REACT_APP_API_URL}/sensor`;

  return {
    getAll: getAll(),
    getOtherPosts: getOtherPosts(),
    getType: getType(),
    getZone: getZone(),
    getCategories: getCategories(),
    getCategoryById: getCategoryById(),
    getStats: getStats(),
    createPost: createPost(),
    filterPosts: filterPosts(),
    addFiltersCategory: addFiltersCategory(),
    addFilterMyPosts: addFilterMyPosts(),
    removeFiltersCategory: removeFiltersCategory(),
    removeAllFiltersCategory: removeAllFiltersCategory(),
    getGeoJsonPosts: getGeoJsonPosts(),
    myPosts: myPosts(),
    filterMyPost: filterMyPost()
  };

  function getAll() {
    return createAsyncThunk(
      `${name}/getAll`,
      async () =>
        await fetchWrapper.get(
          `${baseUrl}/posts?limit=20&sortField=published_at&sortDirection=desc`
        )
    );
  }

  function myPosts() {
    return createAsyncThunk(
      `${name}/myPosts`,
      async () =>
        await fetchWrapper.get(
          `${baseUrl}/users/current/posts?limit=20&sortField=published_at&sortDirection=desc`
        )
    );
  }

  function getOtherPosts() {
    return createAsyncThunk(
      `${name}/getOtherPosts`,
      async (arg) => await fetchWrapper.get(`${arg.next}`)
    );
  }

  function getCategoryById() {
    return createAsyncThunk(
      `${name}/getCategoryById`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/posts?categories=${arg}&limit=1`)
    );
  }

  function getType() {
    return createAsyncThunk(
      `${name}/getType`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/types`)
    );
  }

  function getZone() {
    return createAsyncThunk(
      `${name}/getZone`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/areas?limit=50`)
    );
  }

  function getCategories() {
    return createAsyncThunk(
      `${name}/getCategories`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/categories?limit=100`)
    );
  }

  function getStats() {
    return createAsyncThunk(
      `${name}/getStats`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/stats/status?interval=yearly&format=table`)
    );
  }

  function createPost() {
    return createAsyncThunk(
      `${name}/createPost`,
      async (data) => await fetchWrapper.post(`${baseUrl}/posts`, data)
    );
  }

  function filterPosts() {
    return createAsyncThunk(
      `${name}/addFiltersPost`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/posts?categories=${params}&limit=20&sortField=published_at&sortDirection=desc`
        )
    );
  }

  function addFilterMyPosts() {
    return createAsyncThunk(
      `${name}/myPosts`,
      async (params) =>
        await fetchWrapper.get(
          `${baseUrl}/users/current/posts?categories=${params}&limit=20&sortField=published_at&sortDirection=desc`
        )
    );
  }

  function addFiltersCategory() {
    return createAsyncThunk(`${name}/addFiltersCategory`, (arg) => arg);
  }

  function removeFiltersCategory() {
    return createAsyncThunk(`${name}/removeFiltersCategory`, (arg) => arg);
  }

  function removeAllFiltersCategory() {
    return createAsyncThunk(`${name}/removeAllFiltersCategory`, (arg) => arg);
  }

  function filterMyPost() {
    return createAsyncThunk(`${name}/filterMyPost`, (arg) => arg);
  }

  function getGeoJsonPosts() {
    return createAsyncThunk(`${name}/getGeoJsonPosts`, async (params) => {
      if (params?.auth) {
        return await fetchWrapper.getGeoJson(
          `${baseUrl}/users/current/posts?limit=100&sortField=published_at&sortDirection=desc&categories=${params.categories}`
        );
      } else {
        return await fetchWrapper.getGeoJson(
          `${baseUrl}/posts?limit=100&sortField=published_at&sortDirection=desc&categories=${params.categories}`
        );
      }
    });
  }
}
