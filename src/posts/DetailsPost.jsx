import dayjs from 'dayjs';
import {
  Badge,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Col,
  Container,
  FormGroup,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Spinner,
  TextArea,
  useNavScroll
} from 'design-react-kit';
import { t } from 'i18next';
import { useEffect, useRef, useState } from 'react';
import { MapContainer } from 'react-leaflet';
import { useSelector, useDispatch } from 'react-redux';

import { currentUserActions, detailPostActions } from '../_store';
import { Link, useParams } from 'react-router-dom';
import { ButtonsStatus } from '../_components/Button/ButtonStatus';
import { Comment } from '../_components/Comment/Comment';
import { ImageHeader } from '../_components/Images/ImageHeader';
import { MapCustom } from '../_components/Map/MapCustom';
import { capitalizeFirstLetter } from '../_helpers/utilities';

export function DetailsPost() {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((x) => x.currentUser);
  const { detailPost } = useSelector((x) => x.detailPost);
  let { id } = useParams();
  const containerRef = useRef(null);
  const ref = useRef(null);

  const { register, isActive } = useNavScroll({
    root: containerRef.current || undefined
  });
  const getActiveClass = (id) => (isActive(id) ? 'active nav-link' : 'nav-link');

  // Modal
  const [open, toggleModal] = useState(false);
  const [message, setMessage] = useState(null);

  const toggle = () => toggleModal(!open);
  const openModal = () => {
    setMessage(message);
    toggleModal(true);
  };

  function saveComment() {
    toggle();
    const data = {
      postId: detailPost.id,
      text: message
    };
    if (message) {
      dispatch(detailPostActions.createComment(data))
        .unwrap()
        .then(() => {
          setMessage('');
          dispatch(detailPostActions.getDetailPost({ id }));
        });
    }
  }

  useEffect(() => {
    if (id) {
      dispatch(detailPostActions.getDetailPost({ id })).unwrap();
      dispatch(currentUserActions.getCurrentUser());
    }
  }, [dispatch, id]);

  return (
    <>
      {detailPost.id ? (
        <>
          <Container>
            <Row className="justify-content-center">
              <Col className="col-12" lg={10}>
                <Breadcrumb className={'cmp-breadcrumbs'}>
                  <BreadcrumbItem>
                    <Link to="/"> {t('reports')} </Link>
                    <span className="separator">/</span>
                  </BreadcrumbItem>
                  <BreadcrumbItem active>{detailPost.id}</BreadcrumbItem>
                </Breadcrumb>
              </Col>
            </Row>
          </Container>
          <Container id="intro" tag="section">
            <Row className="justify-content-center mb-40 mb-lg-80">
              <Col className="col-12 pb-2" lg={10}>
                <h1>{detailPost.subject}</h1>
                <h3 className={'mt-2'}>
                  {capitalizeFirstLetter(detailPost.type)}
                  {' n. '}
                  {detailPost.id}
                </h3>
                <Badge color="primary" pill>
                  <ButtonsStatus status={detailPost.status}></ButtonsStatus>
                </Badge>
              </Col>
              <Col xs={'12'}>
                <hr className="d-none d-lg-block mt-30 mb-2" />
              </Col>
            </Row>
            <Row>
              <Col xs={12} lg={3}>
                {/*             <div className="cmp-navscroll sticky-top" aria-labelledby="accordion-title-one">
                  <Navbar
                    expand="lg"
                    className="it-navscroll-wrapper navbar-expand-lg it-top-navscroll it-right-side affix-top"
                  >
                    <div className="navbar-custom" id="navbarNavProgress">
                      <div className="menu-wrapper">
                        <div className="link-list-wrapper">
                          <div className="accordion">
                            <div className="accordion-item">
                              <span className="accordion-header" id="accordion-title-one">
                                <button
                                  className="accordion-button pb-10 px-3 w-100"
                                  type="button"
                                  data-bs-toggle="collapse"
                                  data-bs-target="#collapse-one"
                                  aria-expanded={collapseOpen1}
                                  aria-controls="collapse-one"
                                  onClick={() => setCollapseOpen1(!collapseOpen1)}
                                >
                                  {t('index_page')}
                                  {!collapseOpen1 ? (
                                    <Icon size={'xs'} icon={'it-collapse'}></Icon>
                                  ) : (
                                    <Icon size={'xs'} icon={'it-expand'}></Icon>
                                  )}
                                </button>
                              </span>
                              <div className="progress">
                                <div
                                  className="progress-bar it-navscroll-progressbar"
                                  role="progressbar"
                                  aria-valuenow="10"
                                  aria-valuemin="0"
                                  aria-valuemax="100"
                                ></div>
                              </div>
                              <div
                                id="collapse-one"
                                className={getCollapseClass()}
                                role="region"
                                aria-labelledby="accordion-title-one"
                              >
                                <div className="accordion-body">
                                  <ul className="link-list" data-element="page-index">
                                    {detailPost.description ? (
                                      <li className="nav-item">
                                        <a
                                          href="#description"
                                          className={getActiveClass('description')}
                                          onClick={() => isActive('description')}
                                        >
                                          <span className="title-medium">{t('description')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost.address ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('address')}
                                          href="#address"
                                          onClick={() => isActive('address')}
                                        >
                                          <span className="title-medium">{t('address')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost.images.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('images')}
                                          href="#images"
                                          onClick={() => isActive('images')}
                                        >
                                          <span className="title-medium">{t('report_photo')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.comments.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('comments')}
                                          href="#comments"
                                          onClick={() => isActive('comments')}
                                        >
                                          <span className="title-medium">{t('comments')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.attachments.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('attachments')}
                                          href="#attachments"
                                          onClick={() => isActive('attachments')}
                                        >
                                          <span className="title-medium">{t('attachments')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.approvers.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('contacts')}
                                          href="#contacts"
                                          onClick={() => isActive('contacts')}
                                        >
                                          <span className="title-medium">{t('contacts')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.categories.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('topics')}
                                          href="#topics"
                                          onClick={() => isActive('topics')}
                                        >
                                          <span className="title-medium">{t('topics')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Navbar>
                </div> */}
                <div className="cmp-navscroll sticky-top" aria-labelledby="accordion-title-one">
                  <nav
                    className="navbar it-navscroll-wrapper navbar-expand-lg"
                    aria-label={t('info')}
                    data-bs-navscroll=""
                  >
                    <div className="navbar-custom" id="navbarNavProgress">
                      <div className="menu-wrapper">
                        <div className="link-list-wrapper">
                          <div className="accordion">
                            <div className="accordion-item">
                              <span className="accordion-header" id="accordion-title-one">
                                <button
                                  className="accordion-button pb-10 px-3"
                                  type="button"
                                  data-bs-toggle="collapse"
                                  data-bs-target="#collapse-one"
                                  aria-expanded="true"
                                  aria-controls="collapse-one"
                                  data-focus-mouse="false"
                                >
                                  {t('info')}
                                  <Icon icon={'it-expand'} size={'xs'} className={'right'}></Icon>
                                </button>
                              </span>
                              <div className="progress">
                                <div
                                  className="progress-bar it-navscroll-progressbar"
                                  role="progressbar"
                                  aria-valuenow="0"
                                  aria-valuemin="0"
                                  aria-valuemax="100"
                                ></div>
                              </div>
                              <div
                                id="collapse-one"
                                className="accordion-collapse collapse show"
                                role="region"
                                aria-labelledby="accordion-title-one"
                              >
                                <div className="accordion-body">
                                  <ul className="link-list" data-element="page-index">
                                    {detailPost.description ? (
                                      <li className="nav-item">
                                        <a
                                          href="#description"
                                          className={getActiveClass('description')}
                                          onClick={() => isActive('description')}
                                        >
                                          <span className="title-medium">{t('description')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost.address ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('address')}
                                          href="#address"
                                          onClick={() => isActive('address')}
                                        >
                                          <span className="title-medium">{t('address')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost.images.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('images')}
                                          href="#images"
                                          onClick={() => isActive('images')}
                                        >
                                          <span className="title-medium">{t('report_photo')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.comments.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('comments')}
                                          href="#comments"
                                          onClick={() => isActive('comments')}
                                        >
                                          <span className="title-medium">{t('comments')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.attachments.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('attachments')}
                                          href="#attachments"
                                          onClick={() => isActive('attachments')}
                                        >
                                          <span className="title-medium">{t('attachments')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.approvers.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('contacts')}
                                          href="#contacts"
                                          onClick={() => isActive('contacts')}
                                        >
                                          <span className="title-medium">{t('contacts')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                    {detailPost._embedded.categories.length ? (
                                      <li className="nav-item">
                                        <a
                                          className={getActiveClass('topics')}
                                          href="#topics"
                                          onClick={() => isActive('topics')}
                                        >
                                          <span className="title-medium">{t('topics')}</span>
                                        </a>
                                      </li>
                                    ) : null}
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
              </Col>
              <Col xs={12} lg={8} className={'offset-lg-1'}>
                {detailPost.description ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('description')}>
                      {t('description')}
                    </h2>
                    <p className={'mt-3'}>{detailPost.description}</p>
                    <div>
                      <p>
                        <small className="text-muted">
                          {t('published_at') +
                            ' ' +
                            dayjs(detailPost.published_at).format('DD/MM/YYYY  HH:mm')}
                        </small>
                      </p>
                    </div>
                  </div>
                ) : null}

                {detailPost.address ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('address')}>
                      {t('address')}
                    </h2>
                    <p>
                      {t('address')}: {detailPost.address.address}
                    </p>
                    <MapContainer
                      center={[detailPost.address.latitude, detailPost.address.longitude]}
                      zoom={20}
                      scrollWheelZoom={false}
                      style={{ height: '500px' }}
                    >
                      <MapCustom
                        position={[detailPost.address.latitude, detailPost.address.longitude]}
                      />
                    </MapContainer>
                  </div>
                ) : null}
                {detailPost.images.length ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('images')}>
                      {t('report_photo')}
                    </h2>
                    <Col className="d-lg-flex gap-2 row">
                      {detailPost.images.map((img, idx) => (
                        <ImageHeader props={img} key={`image-${idx.toString()}`}></ImageHeader>
                      ))}
                    </Col>
                  </div>
                ) : null}

                {detailPost._embedded.comments.length ? (
                  <div className="it-page-section mb-50 mb-lg-90">
                    <div className="cmp-card">
                      <div className="card">
                        <div className="card-header border-0 p-0 mb-lg-30 m-0">
                          <div className="d-flex">
                            <h2 className="title-xxlarge mb-3" {...register('comments')}>
                              {t('comments')}
                            </h2>
                          </div>
                        </div>
                        <div className="card-body p-0">
                          {detailPost._embedded.comments.map((comment, idx) => (
                            <Comment
                              comment={comment}
                              postId={detailPost.id}
                              key={comment.id + idx.toString()}
                            />
                          ))}
                          {detailPost._embedded.responses.map((response, idx) => (
                            <Comment
                              operator={true}
                              comment={response}
                              postId={detailPost.id}
                              key={response.id + idx.toString()}
                            />
                          ))}
                        </div>
                        {currentUser?.can_comment ? (
                          <div className="card-footer p-0">
                            <button
                              type="button"
                              onClick={() => openModal({})}
                              className="btn btn-outline-primary w-100 mt-3"
                              data-focus-mouse="false"
                            >
                              <span className="rounded-icon">
                                <Icon color="primary" icon="it-pencil" size={'sm'} />
                              </span>
                              <span>{t('add_comment')}</span>
                            </button>
                          </div>
                        ) : null}
                      </div>
                    </div>
                  </div>
                ) : null}

                {detailPost._embedded.attachments.length ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('attachments')}>
                      {t('attachments')}
                    </h2>
                    <Col xs={'12'}>
                      {detailPost._embedded.attachments.map((att, idx) => (
                        <div
                          className="cmp-icon-link"
                          key={`download_attachments_${idx.toString()}`}
                        >
                          <a
                            className="list-item icon-left d-inline-block"
                            href={att}
                            aria-label={t('download_attachments')}
                            title={t('download_attachments')}
                          >
                            <span className="list-item-title-icon-wrapper">
                              <Icon color={'primary'} icon="it-clip" size={'sm'}></Icon>
                              <span className="list-item">{t('download_attachments')}</span>
                            </span>
                          </a>
                        </div>
                      ))}
                    </Col>
                  </div>
                ) : null}

                {detailPost._embedded.approvers.length ? (
                  <div className={'mb-30'}>
                    <h2 className="title-xxlarge mb-3" {...register('contacts')}>
                      {t('contacts')}
                    </h2>
                    <Col xs={'12'} md={'8'} lg={'6'}>
                      <div className="card-wrapper rounded h-auto pb-0">
                        <div className="card card-teaser card-teaser-info rounded shadow-sm p-3">
                          <div className="card-body pe-3">
                            <span className="title-small">
                              <span className="text-decoration-none" data-element="service-area">
                                {detailPost._embedded.approvers[0].name}
                              </span>
                            </span>
                            <p className="subtitle-small mb-0">
                              {detailPost._embedded.approvers[0].description}
                            </p>
                          </div>
                          <div className="avatar size-xl">
                            <img src="https://picsum.photos/200/200" alt="Immagine" />
                          </div>
                        </div>
                      </div>
                    </Col>
                  </div>
                ) : null}
                {detailPost._embedded.categories.length ? (
                  <Col xs={'12'} className="mb-30" {...register('topics')}>
                    <span className="text-paragraph-small">{t('topics')}:</span>
                    <div className="d-flex flex-wrap gap-2 mt-10 mb-30">
                      {detailPost._embedded.categories.map((topic, index) => (
                        <div className="cmp-tag" key={topic.name + '_' + index.toString()}>
                          <div
                            className="chip chip-simple t-primary bg-tag text-decoration-none"
                            data-element="service-topic"
                          >
                            <span className="chip-label">{topic.name}</span>
                          </div>
                        </div>
                      ))}
                    </div>
                    <p className="text-paragraph-small mb-0">
                      {t('report_updated_at')}
                      {' ' + dayjs(detailPost.modified_at).format('DD/MM/YYYY  HH:mm')}
                    </p>
                  </Col>
                ) : null}
                <Modal fade centered isOpen={open} toggle={toggle} labelledBy="addComment">
                  <ModalHeader toggle={toggle} id="addComment">
                    {t('add_comment')}
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup>
                      <TextArea
                        name="text"
                        rows={3}
                        label={t('message') + ':'}
                        onChange={(e) => setMessage(e.target.value)}
                        value={message || ''}
                        innerRef={ref}
                      />
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={toggle}>
                      {t('close')}
                    </Button>
                    <Button color="primary" onClick={() => saveComment()}>
                      {t('save_changes')}
                    </Button>
                  </ModalFooter>
                </Modal>
              </Col>
            </Row>
          </Container>
        </>
      ) : (
        <Container>
          <Row className="justify-content-center text-center m-3">
            <Col className="col-12">
              <Spinner double active />
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
}
