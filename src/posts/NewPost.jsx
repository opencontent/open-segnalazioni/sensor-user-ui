import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  Dimmer,
  Fade,
  Icon,
  Input,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader, NotificationManager, notify,
  Row,
  Select,
  Spinner,
  StepperContainer,
  StepperHeader,
  StepperHeaderElement,
  TextArea
} from "design-react-kit";
import { t } from 'i18next';
import { useEffect, useRef, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { MapContainer, TileLayer } from 'react-leaflet';
import { useSelector, useDispatch } from 'react-redux';

import { Link, useNavigate } from 'react-router-dom';
import { MapCustom } from '../_components/Map/MapCustom';
import { ImageList } from '../_components/Upload/ImageList';
import { currentUserActions, detailPostActions, postsActions } from '../_store';

export { NewPost };

function NewPost() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { currentUser } = useSelector((x) => x.currentUser);
  const [activeStep, toggleStep] = useState('1');
  const { posts } = useSelector((x) => x.posts);

  //IMAGES
  const [img, setImages] = useState([]);
  const { images } = useSelector((x) => x.images);

  // Modal
  const [isOpenMap, toggleModalMap] = useState(false);

  // MAP
  const [position, setPosition] = useState([]);
  const { types, zones } = useSelector((x) => x.posts);
  const { address } = useSelector((x) => x.detailPost);

  const inputRef = useRef(null);

  const handleClick = () => {
    // 👇️ open file input box on click of other element
    inputRef.current.click();
  };

  const handleFileChange = (event) => {
    if(images.length <= 2){
      const fileObj = Array.from(event.target.files);

      if (!fileObj) {
        return;
      }
      setImages([...img, ...fileObj]);

    }else{
      notify(
        t('file_limit_reached'),
        t('file_limit_reached_error'),
        {
          dismissable: true,
          state: 'warning'}
      )
    }

  };

  function getLocation() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let lat = position.coords.latitude;
        let long = position.coords.longitude;
        setPosition([lat, long]);
        toggleModalMap(!isOpenMap);
      },
      (errors) => {
        console.log(errors);
      }
    );
  }

  function onchangeAddress(e) {
    if (e.target.value === '') {
      dispatch(
        detailPostActions.getAddress({
          address: ''
        })
      );
    } else {
      setValue('address', e.target.value);
    }
  }

  const {
    formState: { errors, isDirty, isValid },
    control,
    reset,
    setValue,
    getValues
  } = useForm({
    defaultValues: {
      subject: '',
      address: '',
      type: '',
      description: '',
      zone: '',
      phone: ''
    }
  });

  const onSubmit = () => {
    let addr;
    if (address.hasOwnProperty('latitude')) {
      addr = { ...address };
    } else {
      addr = getValues('address');
    }

    let data = {
      ...getValues(),
      type: getValues().type.value,
      address: addr,
      images: images
    };

    return dispatch(postsActions.createPost(data))
    .then((response) => {
      if (response?.payload?.id) {
        reset();
        navigate(`/sensor/posts/thankyou?id=${response.payload.id}`);
      }
    })
  };

  const onNext = () => {
    toggleStep('2');
  };

  const onPrev = () => {
    toggleStep('1');
  };

  const optionsZone = () => {
    if (zones?.count > 0) {
      // Change key items for select input pattern
      return zones.items.map(({ id: value, name: label, ...rest }) => ({
        value,
        label,
        ...rest
      }));
    }
    return [];
  };

  const optionsType = () => {
    if (types?.count > 0) {
      // Change key items for select input pattern
      return types.items.map(({ identifier: value, name: label, ...rest }) => ({
        value,
        label,
        ...rest
      }));
    }
    return [];
  };

  function saveAddress() {
    toggleModalMap(!isOpenMap);
    dispatch(
      detailPostActions.getAddress({
        address: address.address,
        latitude: address.latitude,
        longitude: address.longitude
      })
    );
    setValue('address', address.address);
  }

  function closeModalAddress() {
    toggleModalMap(!isOpenMap);
    dispatch(
      detailPostActions.getAddress({
        address: ''
      })
    );
    setValue('address', '');
  }

  useEffect(() => {
    dispatch(postsActions.getType());
    dispatch(postsActions.getZone());
    dispatch(currentUserActions.getCurrentUser());
  }, [dispatch]);

  return (
    <Container>
      <Row className="justify-content-center">
        <Col className="col-12" lg={10}>
          <Breadcrumb className={'cmp-breadcrumbs'}>
            <BreadcrumbItem>
              <Link to="/"> {t('reports')} </Link>
              <span className="separator">/</span>
            </BreadcrumbItem>
            <BreadcrumbItem active>{t('malfunction_reporting')}</BreadcrumbItem>
          </Breadcrumb>
          <div className="cmp-heading pb-3 pb-lg-4">
            <h1 className="title-xxxlarge">{t('malfunction_reporting')}</h1>
          </div>
        </Col>
        <Col className="col-12 cmp-info-progress">
          <StepperContainer className={'mb-lg-80'}>
            <StepperHeader>
              {/*               <StepperHeaderElement variant='confirmed' icon='it-check'>
                Label Step 1
              </StepperHeaderElement> */}
              <StepperHeaderElement
                variant={activeStep === '1' ? 'active' : 'confirmed'}
                appendIcon={activeStep === '2' ? 'it-check' : ''}
              >
                Dati di segnalazione
              </StepperHeaderElement>
              <StepperHeaderElement variant={activeStep === '2' ? 'active' : 'confirmed'}>
                Riepilogo
              </StepperHeaderElement>
              <StepperHeaderElement variant="mobile">{activeStep}/2</StepperHeaderElement>
            </StepperHeader>
          </StepperContainer>
        </Col>
      </Row>
      <Row>
        <div className="col-12 col-lg-3 d-lg-block mb-4 d-none">
          <div className="cmp-navscroll sticky-top" aria-labelledby="accordion-title-one">
            <nav
              className="navbar it-navscroll-wrapper navbar-expand-lg"
              aria-label={t('info')}
            >
              <div className="navbar-custom" id="navbarNavProgress">
                <div className="menu-wrapper">
                  <div className="link-list-wrapper">
                    <div className="accordion">
                      <div className="accordion-item">
                        <span className="accordion-header" id="accordion-title-one">
                          <div className="accordion-button pb-10 px-3" color={'primary'}>
                            {t('info')}
                            {/*        <Icon
                              icon={'it-expand'}
                              size={'xs'}
                              color={'primary'}
                              className={'right'}></Icon> */}
                          </div>
                        </span>
                        <div className="progress">
                          <div
                            className="progress-bar it-navscroll-progressbar"
                            role="progressbar"
                            aria-valuenow="0"
                            aria-valuemin="0"
                            aria-valuemax="100"
                          ></div>
                        </div>
                        <div
                          id="collapse-one"
                          className="accordion-collapse collapse show"
                          role="region"
                          aria-labelledby="accordion-title-one"
                        >
                          <div className="accordion-body">
                            <ul className="link-list" data-element="page-index">
                              <li className="nav-item">
                                <a className="nav-link active" href="#report-place">
                                  <span className="title-medium">{t('place')}</span>
                                </a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link" href="#report-info">
                                  <span className="title-medium">{t('malfunction')}</span>
                                </a>
                              </li>
                              <li className="nav-item">
                                <a className="nav-link" href="#report-author">
                                  <span className="title-medium">{t('report_author')}</span>
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <div className="col-12 col-lg-8 offset-lg-1">
          {activeStep === '1' ? (
            <div>
              <div className="steppers-content" aria-live="polite">
                <div className="it-page-sections-container">
                  <section className="it-page-section" id="report-place">
                    <div className="cmp-card mb-40">
                      <div className="card has-bkg-grey shadow-sm p-big p-lg-4">
                        <div className="card-header border-0 p-0 mb-lg-30 m-0">
                          <div className="d-flex">
                            <h2 className="title-xxlarge mb-1 icon-required">{t('place')}</h2>
                          </div>
                          <p className="subtitle-small mb-0">{t('place_malfunction')}</p>
                        </div>
                        <div className="card-body p-0">
                          <div className="cmp-input-autocomplete">
                            <Controller
                              name="address"
                              control={control}
                              rules={{ required: true }}
                              render={({ field }) => (
                                <div className="form-group bg-white p-3 mb-0 mt-3">
                                  <label htmlFor="address-group"></label>
                                  <input
                                    type="text"
                                    className={'form-control'}
                                    id="address-group"
                                    ref={field.ref}
                                    onChange={(e) => {
                                      field.onChange(e);
                                      onchangeAddress(e);
                                    }}
                                    value={field.value}
                                    placeholder={t('placeholder_address_report')}
                                  />
                                  <div className="link-wrapper mt-3">
                                    <span className="list-item active icon-left cursor-pointer">
                                      <span className="list-item-title-icon-wrapper">
                                        <Icon
                                          icon={'it-map-marker'}
                                          size={'sm'}
                                          color={'primary'}
                                          className={'mb-1'}
                                        ></Icon>
                                        <span
                                          className="list-item-title"
                                          onClick={() => getLocation()}
                                        >
                                          {t('use_position')}
                                        </span>
                                      </span>
                                    </span>
                                  </div>
                                </div>
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section className="it-page-section" id="report-info">
                    <div className="cmp-card mb-40">
                      <div className="card has-bkg-grey shadow-sm p-big">
                        <div className="card-header border-0 p-0 mb-lg-30 m-0">
                          <div className="d-flex">
                            <h2 className="title-xxlarge mb-3 icon-required">{t('malfunction')}</h2>
                          </div>
                        </div>
                        <div className="card-body p-0">
                          <div className="select-wrapper p-md-3 p-lg-4 pb-lg-0 select-partials">
                            <Controller
                              name="type"
                              control={control}
                              rules={{ required: true }}
                              render={({ field }) => (
                                <Select
                                  id="selectType"
                                  options={optionsType()}
                                  placeholder={t('type_report') + '*'}
                                  aria-label={t('placeholder_type_report')}
                                  invalid={errors.type?.type === 'required' ? true : false}
                                  infoText={
                                    errors.type?.type === 'required' ? t('required_field') : false
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                />
                              )}
                            />
                            {errors.type?.type === 'required' ? (
                              <small className="invalid-feedback d-block form-text ">
                                {t('required_field')}
                              </small>
                            ) : null}
                          </div>
                          <div className="select-wrapper p-md-3 p-lg-4 pb-lg-0 select-partials">
                            <Controller
                              name="zone"
                              control={control}
                              rules={{ required: true }}
                              render={({ field }) => (
                                <Select
                                  id="selectZone"
                                  options={optionsZone()}
                                  placeholder={t('placeholder_reporting_area')}
                                  aria-label={t('placeholder_reporting_area')}
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                />
                              )}
                            />
                            {errors.zone?.type === 'required' ? (
                              <small className="invalid-feedback d-block form-text ">
                                {t('required_field')}
                              </small>
                            ) : null}
                          </div>
                          <div className="text-area-wrapper p-3 px-lg-4 pt-lg-4 pb-lg-0 bg-white">
                            <Controller
                              name="subject"
                              control={control}
                              rules={{ required: true }}
                              render={({ field }) => (
                                <Input
                                  type="text"
                                  className={'cmp-input mb-0'}
                                  label={t('title') + '*'}
                                  placeholder={''}
                                  invalid={errors.subject?.type === 'required' ? true : false}
                                  infoText={
                                    errors.subject?.type === 'required'
                                      ? t('required_field')
                                      : false
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                />
                              )}
                            />
                          </div>
                          <div className="cmp-text-area m-0 p-3 px-lg-4 pt-lg-5 pb-lg-4 bg-white">
                            <Controller
                              name="description"
                              control={control}
                              rules={{ required: true }}
                              render={({ field }) => (
                                <TextArea
                                  className={'border-none'}
                                  label={t('description') + '*'}
                                  rows={2}
                                  placeholder={''}
                                  invalid={errors.description?.type === 'required' ? true : false}
                                  infoText={
                                    errors.description?.type === 'required'
                                      ? t('required_field')
                                      : t('enter_max_200_char')
                                  }
                                  innerRef={field.ref}
                                  value={field.value}
                                  onChange={field.onChange}
                                />
                              )}
                            />
                          </div>
                          <div className="btn-wrapper px-3 pt-2 pb-3 px-lg-4 pb-lg-4 pt-lg-0 bg-white">
                            <label className="title-xsmall-bold u-grey-dark pb-2 ms-2">
                              {t('images')}
                            </label>
                            {img.map((img, index) => (
                              <ImageList img={img} key={index + 'img'}></ImageList>
                            ))}
                            <button
                              type="button"
                              aria-label="Carica file per il disservizio"
                              className="btn btn-primary w-100 fw-bold"
                              onClick={handleClick}
                            >
                              <input
                                ref={inputRef}
                                type="file"
                                onChange={handleFileChange}
                                accept="image/*"
                                className="d-none upload"
                                multiple="multiple"
                              />
                              <span className="rounded-icon">
                                <Icon
                                  icon={'it-upload'}
                                  size={'sm'}
                                  color={'primary'}
                                  className={'icon-white'}
                                ></Icon>
                              </span>
                              <span className="">{t('load_file')}</span>
                            </button>
                            <p className="title-xsmall u-grey-dark pt-10 mb-0">
                              {t('select_max_images')}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>

                  <section className="it-page-section" id="report-author">
                    <div className="cmp-card">
                      <div className="card has-bkg-grey shadow-sm">
                        <div className="card-header border-0 p-0 mb-lg-30 m-0">
                          <div className="d-flex">
                            <h2 className="title-xxlarge mb-1">{t('report_author')}</h2>
                          </div>
                          <p className="subtitle-small mb-0">{t('my_info')}</p>
                        </div>
                        <div className="card-body p-0">
                          <div className="cmp-info-button-card mt-3">
                            <div className="card p-3 p-lg-4">
                              <div className="card-body p-0">
                                <h3 className="big-title mb-0">
                                  {currentUser?.first_name ? currentUser?.first_name : '--'} {currentUser?.last_name}
                                </h3>

                                <p className="card-info">
                                  {t('fiscal_code')} <br />
                                  <span>{currentUser?.fiscal_code ? currentUser?.fiscal_code : '--'}</span>
                                </p>
                                <div className="accordion-item">
                                  <div className="accordion-header" id="heading-collapse-parents">
                                    <button
                                      className="collapsed accordion-button"
                                      type="button"
                                      data-bs-toggle="collapse"
                                      data-bs-target="#collapse-parents"
                                      aria-expanded="false"
                                      aria-controls="collapse-parents"
                                      data-focus-mouse="false"
                                    >
                                      <span className="d-flex align-items-center">
                                        {t('show_more')}
                                    {/*     <Icon
                                          icon={'it-expand'}
                                          size={'sm'}
                                          color={'primary'}
                                        ></Icon> */}
                                      </span>
                                    </button>
                                  </div>
                                  <div
                                    id="collapse-parents"
                                    className="accordion-collapse collapse"
                                    role="region"
                                  >
                                    <div className="accordion-body p-0">
                                      <div className="cmp-info-summary bg-white has-border">
                                        <Card>
                                          <CardHeader className="border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                                            <h4 className="title-large-semi-bold mb-3">{t('contacts')}</h4>
                                            <Link
                                              to="_components/Card/CardPost#q"
                                              className="d-none text-decoration-none"
                                            >
                                              <span className="text-button-sm-semi t-primary">
                                                {t('edit')}
                                              </span>
                                            </Link>
                                          </CardHeader>
                                          <CardBody className="p-0">
                                            <div className="single-line-info border-light">
                                              <div className="text-paragraph-small">{t('phone')}</div>
                                              <div className="border-light">
                                                <p className="data-text">{currentUser?.phone ? currentUser?.phone : '--'}</p>
                                              </div>
                                            </div>
                                            <div className="single-line-info border-light">
                                              <div className="text-paragraph-small">Email</div>
                                              <div className="border-light">
                                                <p className="data-text">{currentUser?.email ? currentUser?.email : '--'}</p>
                                              </div>
                                            </div>
                                          </CardBody>
                                        </Card>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          ) : null}
          {activeStep === '2' ? (
            <div>
              <div className="steppers-content" aria-live="polite">
                <div className="it-page-sections-container">
                  <div className="callout callout-highlight ps-3 warning">
                    <div className="callout-title mb-20 d-flex align-items-center">
                      <Icon icon={'it-horn'} size={'sm'}></Icon>
                      <span>Attenzione</span>
                    </div>
                    <p className="titillium text-paragraph">
                      {t('alert_info_post')}
                      <span className="d-lg-block"> {t('alert_info_post_verify')}</span>
                    </p>
                  </div>
                  <h2 className="title-xxlarge mb-4 mt-40">{t('reporting')}</h2>

                  <div className="cmp-card mb-4">
                    <div className="card has-bkg-grey shadow-sm mb-0">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          <h3 className="subtitle-large mb-4">{t('malfunction')}</h3>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="cmp-info-summary bg-white p-3 p-lg-4">
                          <div className="card">
                            <div className="card-header border-bottom border-light p-0 mb-0 pb-2 d-flex justify-content-end">
                              <span className="text-decoration-none" onClick={onPrev}>
                                <span className="text-button-sm-semi t-primary">{t('edit')}</span>
                              </span>
                            </div>

                            <div className="card-body p-0">
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('address')}</div>
                                <div className="border-light">
                                  <p className="data-text">{getValues('address')}</p>
                                </div>
                              </div>
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('type_of_disservice')}</div>
                                <div className="border-light">
                                  <p className="data-text">{getValues('type').value}</p>
                                </div>
                              </div>
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('title')}</div>
                                <div className="border-light">
                                  <p className="data-text">{getValues('subject')}</p>
                                </div>
                              </div>
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('details')}</div>
                                <div className="border-light">
                                  <p className="data-text">{getValues('description')}</p>
                                </div>
                              </div>
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('images')}</div>
                                <div className="border-light">
                                  {img.map((el, index) => (
                                    <p className="data-text" key={'data-text' + index}>
                                      {el.name}
                                    </p>
                                  ))}
                                </div>
                              </div>
                            </div>
                            <div className="card-footer p-0"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <h2 className="title-xxlarge mb-4 mt-40">{t('general_data')}</h2>

                  <div className="cmp-card mb-4">
                    <div className="card has-bkg-grey shadow-sm mb-0">
                      <div className="card-header border-0 p-0 mb-lg-30 m-0">
                        <div className="d-flex">
                          <h3 className="subtitle-large mb-4">{t('report_author')}</h3>
                        </div>
                      </div>
                      <div className="card-body p-0">
                        <div className="cmp-info-summary bg-white mb-4 mb-lg-30 p-3 p-lg-4">
                          <div className="card">
                            <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                              <h3 className="title-large-semi-bold mb-3">
                                {currentUser?.first_name ? currentUser?.first_name : ''} {currentUser?.last_name}
                              </h3>
                            </div>

                            <div className="card-body p-0">
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('fiscal_code')}</div>
                                <div className="border-light">
                                  <p className="data-text">{currentUser?.fiscal_code ? currentUser?.fiscal_code : ''}</p>
                                </div>
                              </div>
                            </div>
                            <div className="card-footer p-0 d-none"></div>
                          </div>
                        </div>
                        <div className="cmp-info-summary bg-white p-3 p-lg-4">
                          <div className="card">
                            <div className="card-header border-bottom border-light p-0 mb-0 d-flex justify-content-between d-flex justify-content-end">
                              <h3 className="title-large-semi-bold mb-3">{t('contacts')}</h3>
                            </div>

                            <div className="card-body p-0">
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">{t('phone')}</div>
                                <div className="border-light">
                                  <p className="data-text">{currentUser?.phone ? currentUser?.phone : ''}</p>
                                </div>
                              </div>
                              <div className="single-line-info border-light">
                                <div className="text-paragraph-small">Email</div>
                                <div className="border-light">
                                  <p className="data-text">{currentUser?.email}</p>
                                </div>
                              </div>
                            </div>
                            <div className="card-footer p-0 d-none"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
          <div className="cmp-nav-steps">
            <nav className="steppers-nav" aria-label="Step">
              <Button
                type="button"
                size={'sm'}
                className="steppers-btn-prev p-0"
                onClick={onPrev}
                disabled={activeStep === '1'}
              >
                <Icon icon={'it-chevron-left'} size={'sm'} color={'primary'}></Icon>
                <span className="text-button-sm">{t('back')}</span>
              </Button>
              {activeStep === '2' ? (
                <Button
                  color="primary"
                  type="button"
                  size={'sm'}
                  className="steppers-btn-confirm"
                  disabled={!isValid || !isDirty}
                  onClick={onSubmit}
                >
                  <span className="text-button-sm">{t('send')}</span>
                  <Icon
                    icon={'it-chevron-right'}
                    size={'sm'}
                    color={'primary'}
                    className={'icon-white'}
                  ></Icon>
                </Button>
              ) : (
                <Button
                  color="primary"
                  type="button"
                  size={'sm'}
                  className="steppers-btn-confirm"
                  disabled={!isValid || !isDirty}
                  onClick={onNext}
                >
                  <span className="text-button-sm">{t('next')}</span>
                  <Icon
                    icon={'it-chevron-right'}
                    size={'sm'}
                    color={'primary'}
                    className={'icon-white'}
                  ></Icon>
                </Button>
              )}
            </nav>
            {posts?.error ? (
              <div
                id="alert-message"
                className="alert alert-danger cmp-disclaimer rounded"
                role="alert"
              >
                <span className="d-inline-block text-uppercase cmp-disclaimer__message">
                      {t('not_authorized')}
                </span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>
      </Row>
      <div>
        <Modal
          keyboard={false}
          backdrop={'static'}
          isOpen={isOpenMap}
          toggle={() => toggleModalMap(!isOpenMap)}
          centered
          labelledBy="modalMap"
        >
          <ModalHeader toggle={() => toggleModalMap(!isOpenMap)} id="modalMap">
            {t('select_map_point')}
          </ModalHeader>
          <ModalBody>
            <MapContainer
              center={position}
              zoom={20}
              scrollWheelZoom={false}
              style={{ height: '500px' }}
            >
              <MapCustom position={position} draggable={true} />
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
            </MapContainer>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => closeModalAddress()}>
              {t('close')}
            </Button>
            <Button color="primary" onClick={() => saveAddress()}>
              {t('save_address')}
            </Button>
          </ModalFooter>
        </Modal>
      </div>
      {posts?.loading ? (
        <Fade className="mt-3 dinner-all-screen">
          <Dimmer color="primary" icon="">
            <div className={'d-flex justify-content-center'}>
              <Spinner active></Spinner>
            </div>
          </Dimmer>
        </Fade>
      ) : null}
      <NotificationManager containerId='error-files' />
    </Container>
  );
}
