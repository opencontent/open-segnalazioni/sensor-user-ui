import { Breadcrumb, BreadcrumbItem, Button, Col, Container, Icon, Row } from 'design-react-kit';
import { t } from 'i18next';
import { useEffect } from 'react';
import { Trans } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import { currentUserActions } from '../_store';
import { Link, useSearchParams } from 'react-router-dom';

export { ThankYouPage };

function ThankYouPage() {
  const dispatch = useDispatch();
  const { currentUser } = useSelector((x) => x.currentUser);
  let [searchParams] = useSearchParams();

  useEffect(() => {
    dispatch(currentUserActions.getCurrentUser());
  }, [dispatch]);

  return (
    <Container>
      <Row className="justify-content-center mb-50">
        <Col className="col-12" lg={10}>
          <Breadcrumb className={'cmp-breadcrumbs'}>
            <BreadcrumbItem>
              <Link to="/"> {t('reports')} </Link>
              <span className="separator">/</span>
            </BreadcrumbItem>
            <BreadcrumbItem active>{t('malfunction_reporting')}</BreadcrumbItem>
          </Breadcrumb>
        </Col>
        <Col className="col-12 pb-2" lg={10}>
          <div className="cmp-heading p-0">
            <div className="categoryicon-top d-flex">
              <Icon color="success" icon="it-check-circle" className={' mr-10 icon-sm mb-1'} />
              <h1 className="title-xxxlarge">{t('report_sent')}</h1>
            </div>
            <p className="subtitle-small">
              <Trans
                t={t}
                values={{ id: searchParams.get('id') | null }}
                i18nKey={'thanks_message_report_sent'}
                components={{ bold: <strong /> }}
              ></Trans>
            </p>
            <p className="subtitle-small">
              <Trans
                t={t}
                i18nKey={'thanks_message_report_sent_description'}
                components={{ Link: <Link /> }}
              ></Trans>
            </p>
            <p className="subtitle-small pt-3 pt-lg-4">
              <Trans t={t} i18nKey={'thanks_message_report_sent_email'}></Trans>
              <br />
              <strong>{currentUser?.email}</strong>
            </p>
            <Button outline color={'primary'} className="bg-white fw-bold">
              <Link
                to={`/sensor/posts/${searchParams.get('id') | null}/pdf`}
                download
                target={'_blank'}
              >
                <span className="rounded-icon">
                  <Icon icon={'it-download'} size={'sm'} color={'primary'}></Icon>
                </span>
                <span>
                  {' '}
                  <Trans t={t} i18nKey={'thanks_message_report_sent_download'}></Trans>
                </span>
              </Link>
            </Button>
          </div>
          <p className="mt-3">
            <Trans
              t={t}
              values={{ id: searchParams.get('id') | null }}
              i18nKey={'thanks_message_report_sent_read_post'}
              components={{ Link: <Link /> }}
            ></Trans>
            <span className="text-paragraph">
              <Trans t={t} i18nKey={'thanks_message_report_sent_read_post_description'}></Trans>
            </span>
          </p>
        </Col>
      </Row>
    </Container>
  );
}
