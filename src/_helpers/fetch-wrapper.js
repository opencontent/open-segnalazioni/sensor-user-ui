import { authActions, store } from '../_store/index';

export const fetchWrapper = {
  get: request('GET'),
  getGeoJson: request('GET', 'application/vnd.geo+json'),
  post: request('POST'),
  put: request('PUT'),
  delete: request('DELETE')
};

function request(method, type) {
  return (url, body) => {
    const requestOptions = {
      method,
      headers: authHeader(url, type)
    };
    if (body) {
      requestOptions.headers['Content-Type'] = 'application/json';
      requestOptions.body = JSON.stringify(body);
    }
    return fetch(url, requestOptions).then(handleResponse);
  };
}

// helper functions

function authHeader(url, type) {
  // return auth header with jwt if user is logged in and request is to the api url
  const token = authToken();
  const isLoggedIn = !!token;
  const isApiUrl = url.startsWith('/api');

  if (isLoggedIn && isApiUrl) {
    return { Authorization: `Bearer ${token}`, Accept: type };
  } else {
    return { Accept: type };
  }
}

function authToken() {
  return store.getState().auth.user?.token;
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);

    if (!response.ok) {
      if ([401, 403, 500].includes(response.status) && authToken()) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        const logout = () => store.dispatch(authActions.logout());
        logout();
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
