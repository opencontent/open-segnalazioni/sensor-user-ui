import { useEffect, useState } from 'react';
import { Routes, Route, Navigate, useNavigate, useLocation } from 'react-router-dom';
//import { PrivateRoute } from "./_components/PrivateRoute";
import WebFont from 'webfontloader';

import { history } from './_helpers/history';
import { Home } from './home/Home';

import { Login } from './login/Login';
import { DetailsPost } from './posts/DetailsPost';
import { NewPost } from './posts/NewPost';
import { ThankYouPage } from './posts/ThankYouPage';

//Set globally BTS-ITALIA Version
import BOOTSTRAP_ITALIA_VERSION from 'bootstrap-italia/dist/version';
window.BOOTSTRAP_ITALIA_VERSION = BOOTSTRAP_ITALIA_VERSION;

export function App({ name, callback }) {
  // init custom history object to allow navigation from
  // anywhere in the react app (inside or outside components)
  history.navigate = useNavigate();
  history.location = useLocation();
  const [showTopBtn, setShowTopBtn] = useState(false);

  useEffect(() => {
    // Importing files depending on env
    if (process.env.REACT_APP_STYLE === 'true') {
      import(`./assets/stylesheets/core.scss`);
      import('./index.scss');
      import('bootstrap-italia/dist/bootstrap-italia.esm');
      WebFont.load({
        custom: {
          families: [
            'Titillium Web:300,400,600,700:latin-ext',
            'Lora:400,700:latin-ext',
            'Roboto Mono:400,700:latin-ext'
          ]
        }
      });
    } else {
      import('./index.scss');
    }
  }, []);

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.innerHeight + window.scrollY >= document.body.scrollHeight) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);

  return (
    <div className="app-container">
      {/* <Nav /> */}
      <div className="container pt-4 pb-4 vh-100">
        <Routes>
          <Route
            path="/sensor/posts"
            element={
              // <PrivateRoute>
              <Home goToTop={showTopBtn} />
              // </PrivateRoute>
            }
          />
          <Route
            path="/sensor/posts/:id"
            element={
              // <PrivateRoute>
              <DetailsPost />
              //</PrivateRoute>
            }
          />
          <Route
            path="/sensor/posts/new"
            element={
              // <PrivateRoute>
              <NewPost />
              //</PrivateRoute>
            }
          />
          <Route
            path="/sensor/posts/thankyou"
            element={
              // <PrivateRoute>
              <ThankYouPage />
              // </PrivateRoute>
            }
          />
          <Route path="/login" element={<Login />} />
          <Route path="/*" element={<Navigate to="/sensor/posts" />} />
        </Routes>
      </div>
    </div>
  );
}
