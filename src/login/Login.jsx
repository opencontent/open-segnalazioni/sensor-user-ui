import { yupResolver } from '@hookform/resolvers/yup';

import { history } from '../_helpers/history';
import { authActions, currentUserActions } from '../_store';
import { Alert, Button, Card, CardBody, CardTitle, Form, FormGroup, Input } from 'design-react-kit';
import { useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';

export { Login };

function Login() {
  const dispatch = useDispatch();
  const authUser = useSelector((x) => x.auth.user);
  const authError = useSelector((x) => x.auth.error);

  useEffect(() => {
    // redirect to home if already logged in
    if (authUser) {
      history.navigate('/');
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // form validation rules
  const validationSchema = Yup.object().shape({
    username: Yup.string().required('Username o e-mail obbligatorio'),
    password: Yup.string().required('Password è obbligatoria')
  });
  const formOptions = {
    resolver: yupResolver(validationSchema),
    defaultValues: {
      username: '',
      password: ''
    }
  };

  // get functions to build form with useForm() hook
  const { handleSubmit, formState, control } = useForm(formOptions);
  const { errors, isSubmitting } = formState;

  function onSubmit({ username, password }) {
    return dispatch(
      authActions.login({
        username,
        password
      })
    ).then(() => {
      dispatch(currentUserActions.getCurrentUser());
    });
  }

  return (
    <div className="col-md-6 offset-md-3 mt-5">
      <Alert color="info">
        Username: admin
        <br />
        Password: Krotalo
      </Alert>
      <Card spacing className="card-bg">
        <CardBody>
          <CardTitle tag={'h4'} className={'text-center'}>
            Login
          </CardTitle>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <div className="">
              <FormGroup className={'col-12'}>
                <label className={'visually-hidden'}>Username o Email</label>
                <Controller
                  name="username"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <Input
                      name="username"
                      type="text"
                      className={`${errors.username ? 'is-invalid mb-0' : ''}`}
                      label={'Username / Email'}
                      {...field}
                    />
                  )}
                />
                <div className="invalid-feedback">{errors.username?.message}</div>
              </FormGroup>
            </div>
            <div className="">
              <FormGroup className={'col'}>
                <label className={'visually-hidden'}>Password</label>
                <Controller
                  name="password"
                  control={control}
                  rules={{ required: true }}
                  render={({ field }) => (
                    <Input
                      name="password"
                      type="password"
                      className={` ${errors.password ? 'is-invalid mb-0' : ''}`}
                      label={'Password'}
                      {...field}
                    />
                  )}
                />
                <div className="invalid-feedback">{errors.password?.message}</div>
              </FormGroup>
            </div>
            <div className={'text-center'}>
              <Button disabled={isSubmitting} color={'primary'} size={'lg'} type={'submit'}>
                {isSubmitting && <span className="spinner-border spinner-border-sm m-auto"></span>}
                Accedi
              </Button>
            </div>
            <div className="form-row col-12 mb-0">
              {authError && (
                <Alert className="mt-3 mb-0" color={'danger'}>
                  {authError.message}
                </Alert>
              )}
            </div>
          </Form>
        </CardBody>
      </Card>
    </div>
  );
}
